package android.humanbooster.kanban;

import android.content.Intent;
import android.graphics.Color;
import android.humanbooster.kanban.helpers.MySingleton;
import android.humanbooster.kanban.models.Project;
import android.humanbooster.kanban.models.ProjectsBaseAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String PROJECT_ID_KEY = "currentProjectId";
    private  ProjectsBaseAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        /*
        // méthode simple avec un ArrayAdapter
        List<Project> list = new ArrayList<>();
        list.add(new Project("Java", "Java course"));
        list.add(new Project("Java2", "Java course2"));

      ArrayAdapter<Project> adapter =
                new ArrayAdapter(this,
                        android.R.layout.simple_list_item_1,
                        list);

        // on récupere la listView du Layout
        ListView itemsListView  = (ListView) findViewById(R.id.listViewProject);
        // on lui fournit l'adapter
        itemsListView.setAdapter(adapter);
        */

        List<Project> list = MySingleton.getInstance().resetProjectList(50);

        adapter = new ProjectsBaseAdapter(this, list);
        ListView itemsListView  = (ListView) findViewById(R.id.listViewProject);
        itemsListView.setAdapter(adapter);

        itemsListView.setOnItemClickListener(taskItemClickListener);


    }

    private AdapterView.OnItemClickListener taskItemClickListener = new AdapterView.OnItemClickListener()
    {
        // attention le layout dans project_list_row ne doit pas être clickable sinon l'event ne se propage pas jusqu'ici
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // view correspond au project_list_row correspondant (attention c'est recyclé)
            // pour comprendre
            // view.setBackgroundColor(Color.BLACK);
            /*
            parent The AdapterView where the click happened.
            view The view within the AdapterView that was clicked (this will be a view provided by the adapter)
            position The position of the view in the adapter.
            id The row id of the item that was clicked.
             */
            // parent ==> listView

            System.out.println("position " + position);

            // On recupere via l'adapter le projet à la position du click
            Project clickedProject = adapter.getItem(position);
            Log.i("DEBUG", clickedProject.toString());

            // on créée l'indent
            Intent t = new Intent(view.getContext(), TaskActivity.class);
            // on lui passe en paramètre l'id du project (pour faire simple)
            // on pourrait passer l'objet directemnt mais il faudrait rendre la class Project Parcelable
            // c'est comme serializable mais en mieux pour les perf
            // http://stackoverflow.com/questions/2139134/how-to-send-an-object-from-one-android-activity-to-another-using-intents
            // http://www.developerphil.com/parcelable-vs-serializable/
            // https://developer.android.com/reference/android/os/Parcelable.html
            // https://guides.codepath.com/android/using-parcelable

            t.putExtra(PROJECT_ID_KEY, clickedProject.getId());
            startActivity(t);
        }
    };




    public void clickDebugBtn(View view) {

        for(String s : MySingleton.getInstance().myList) {
            System.out.println("icic" + s);
        }
    }

    public void clickStart(View view) {
        //startActivity(new Intent(view.getContext(), ProjectActivity.class ));
    }
}
