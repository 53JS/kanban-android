package android.humanbooster.kanban.models;


import android.graphics.Color;
import android.humanbooster.kanban.helpers.State;

public class Task {
    private String name;
    private String description;
    private int color;
    private State state;

    public Task(String name, String description) {
        this(name, description,Color.BLUE ,State.TODO);
    }

    public Task(String name, String description, int color,   State state) {
        this.color = color;
        this.description = description;
        this.name = name;
        this.state = state;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Task{" +
                "color=" + color +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", state=" + state +
                '}';
    }
}
