package android.humanbooster.kanban.models;

import android.content.Context;
import android.humanbooster.kanban.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;



public class ProjectsBaseAdapter extends BaseAdapter {

    private Context context;
    private List<Project> projects;

    public ProjectsBaseAdapter(Context context, List<Project> items) {
        this.context = context;
        this.projects = items;
    }

    @Override
    public int getCount() {
        return projects.size();
    }

    @Override
    public Project getItem(int position) {
        return projects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Project> getProjects() {
        return this.projects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.project_list_row, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Project currentItem = (Project) getItem(position);
        viewHolder.itemName.setText(currentItem.getName());


        return convertView;
    }

    private class ViewHolder {
        TextView itemName;
        TextView itemDescription;

        public ViewHolder(View view) {
            itemName = (TextView) view.findViewById(R.id.name);
            itemDescription = (TextView) view.findViewById(R.id.description);
        }
    }
}
