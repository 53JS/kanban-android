package android.humanbooster.kanban.models;


import android.content.Context;
import android.graphics.Color;
import android.humanbooster.kanban.R;
import android.humanbooster.kanban.helpers.State;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class TasksBaseAdapter  extends BaseAdapter {
    private Context context;
    private List<Task> projects;

    public TasksBaseAdapter(Context context, List<Task> items) {
        this.context = context;
        this.projects = items;
    }

    @Override
    public int getCount() {
        return projects.size();
    }

    @Override
    public Task getItem(int position) {
        return projects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TasksBaseAdapter.ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.task_list_row, parent, false);
            viewHolder = new TasksBaseAdapter.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (TasksBaseAdapter.ViewHolder) convertView.getTag();
        }

        Task currentItem = (Task) getItem(position);
        viewHolder.itemName.setText(currentItem.getName());
        viewHolder.itemDescription.setText(currentItem.getDescription());
        viewHolder.itemState.setText(currentItem.getState().toString());
        convertView.setBackgroundColor(currentItem.getColor());


        return convertView;
    }

    private class ViewHolder {
        TextView itemName;
        TextView itemDescription;
        TextView itemState;
        int itemColor;

        public ViewHolder(View view) {
            itemName = (TextView) view.findViewById(R.id.taskTextViewName);
            itemDescription = (TextView) view.findViewById(R.id.taskTextViewDescriptiojn);
            itemState = (TextView) view.findViewById(R.id.taskTextViewState);

        }
    }
}
