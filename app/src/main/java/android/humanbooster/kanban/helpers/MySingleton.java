package android.humanbooster.kanban.helpers;


import android.graphics.Color;
import android.humanbooster.kanban.models.Project;
import android.humanbooster.kanban.models.Task;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MySingleton {

    private static MySingleton mySingleton;

    public ArrayList<String> myList = new ArrayList<>();
    public List<Project> projectList = new ArrayList<>();

    private MySingleton() {
    }

    public List<Project> resetProjectList(int n) {
        projectList = new ArrayList<>();
        for(int i = 0 ; i< n; i ++) {
            Project temp = new Project("Project Java " + i, "Java course ");


            temp.setTasks(Arrays.asList(
                    new Task("IHM " + i, "améliorer l'IHM "+ i,randColor(), State.TODO),
                    new Task("Structure ", "mettre en place la structure ", randColor(), State.DONE),
                    new Task("Code ", "devs ", randColor(), State.DOING)

            ));
            projectList.add(temp);
        }
        return projectList;
    }

    private int randColor() {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        return color;
    }

    public Project getProjectFromId(int id ) {
        for( Project p: projectList) {
            if (p.getId() == id) {
                return p;
            }
        }
        Log.w("DEBUG", "ATTENTION pas d'id correspondant");
        return null;
    }

    public static MySingleton getInstance() {
        if (mySingleton == null) {
            mySingleton = new MySingleton();
            Log.i("SINGLETON", "Singleton null");
        }
        else {
            Log.i("SINGLETON", "Singleton NOT null");
        }
        return mySingleton;
    }

    public String toString() {
        return Arrays.toString(myList.toArray());
    }


}
