package android.humanbooster.kanban.helpers;



public enum State {
    TODO,
    DOING,
    DONE
}

